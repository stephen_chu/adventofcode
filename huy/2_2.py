import sys


def main(input):
    total = 0
    for line in input:
        dim = line.split('x')
        w = int(dim[0])
        h = int(dim[1])
        l = int(dim[2])
        dim_list = [w, h, l]
        min1 = min(dim_list)
        dim_list.remove(min1)
        min2 = min(dim_list)
        vol = w * h * l
        total += (2 * (min1 + min2)) + vol
    print total

if __name__ == "__main__":
    main(sys.stdin.readlines())
