import sys


def main(input):
    visited = {(0, 0): 1}
    x = 0
    y = 0
    for direction in input:
        if direction == '>':
            x += 1
        elif direction == '<':
            x -= 1
        elif direction == '^':
            y += 1
        elif direction == 'v':
            y -= 1

        coordinate = (x, y)
        if coordinate in visited:
            visited[coordinate] += 1
        else:
            visited[coordinate] = 1

    print len(visited)

if __name__ == "__main__":
    main(sys.stdin.readline())
