import sys


def main(input):
    total = 0
    for line in input:
        dim = line.split('x')
        front = int(dim[0]) * int(dim[1])
        side = int(dim[1]) * int(dim[2])
        top = int(dim[2]) * int(dim[0])
        areas = [front, side, top]
        total += (2 * (front + side + top)) + min(areas)
    print total

if __name__ == "__main__":
    main(sys.stdin.readlines())
