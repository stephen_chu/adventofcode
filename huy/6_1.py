import sys
import re


def main(input):
    grid = {}
    for x in xrange(0, 1000):
        for y in xrange(0, 1000):
            grid[(x, y)] = False

    for line in input:
        m = re.search(r'(.+)\s(\d+),(\d+)\sthrough\s(\d+),(\d+)', line)
        instruction = m.group(1)
        x1 = int(m.group(2))
        y1 = int(m.group(3))
        x2 = int(m.group(4))
        y2 = int(m.group(5))

        if instruction == 'turn on':
            for x in xrange(x1, x2 + 1):
                for y in xrange(y1, y2 + 1):
                    grid[(x, y)] = True
        elif instruction == 'turn off':
            for x in xrange(x1, x2 + 1):
                for y in xrange(y1, y2 + 1):
                    grid[(x, y)] = False
        elif instruction == 'toggle':
            for x in xrange(x1, x2 + 1):
                for y in xrange(y1, y2 + 1):
                    grid[(x, y)] = not grid[(x, y)]

    count = 0
    for x in xrange(0, 1000):
        for y in xrange(0, 1000):
            if grid[(x, y)]:
                count += 1

    print(count)

if __name__ == "__main__":
    main(sys.stdin.readlines())
