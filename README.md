# Advent of Code

## Description
This is just a way to get our Python feet wet (even though pythons have no feet...)

## What to do
1. Fork this repo (Huy's repo) into your own Bitbucket account
2. Clone the forked repo onto your computer
3. Solve problems from [Advent of Code](http://adventofcode.com/)
4. Commit them to your local machine repo then push them to your forked Bitbucket repo *
5. Create a pull request (PR) to Huy's repo from your forked Bitbucket repo

* If you try to commit anything to Huy's repo, you won't be able to since you don't have to the access. This is on purpose. Only PR's will be permitted to Huy's repo.

## Project structure
I've created a directory in the project for each of us. You'll add a ".py" file per problem you've solved. Name it "1_1.py" for the first problem solved on the first day, "1_2.py" for the second problem you solved on the first day, "2_1.py" for the first problem on the second day, and so on. Create a PR for problem you finish.